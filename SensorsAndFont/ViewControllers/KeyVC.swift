//  KeyVC.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko


import UIKit
import Foundation

import KeychainAccess

class KeyVC: UIViewController {
    
    @IBOutlet weak var loginOutlet: UITextField!
    @IBOutlet weak var passwOutlet: UITextField!
    @IBOutlet weak var dataUser: UITextView!
    
    
    @IBAction func addInKey(_ sender: Any) {
        let keychainLogin: Keychain
        if let serviceLogin = loginOutlet.text, !serviceLogin.isEmpty {
            keychainLogin = Keychain(service: serviceLogin)
        } else {
            keychainLogin = Keychain()
        }
        let keychainPassw: Keychain
        if let servicePassw = passwOutlet.text, !servicePassw.isEmpty {
            keychainPassw = Keychain(service: servicePassw)
        } else {
            keychainPassw = Keychain()
        }
        
        
        keychainLogin[loginOutlet.text!] = loginOutlet.text
        keychainPassw[passwOutlet.text!] = passwOutlet.text
        
        dataUser.text = String(keychainLogin[loginOutlet.text!]! + "\n" + keychainPassw[passwOutlet.text!]!)
    }
    
}
