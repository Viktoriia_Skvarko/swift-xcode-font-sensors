//  GalleryVC.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko


import UIKit

class GalleryVC: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textFromPhoto: UILabel!
    
    @IBAction func addPhotoAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let addPhotoAction = UIImagePickerController()
            addPhotoAction.delegate = self
            addPhotoAction.sourceType = .photoLibrary
            
            present(addPhotoAction, animated: true, completion: nil)
            textFromPhoto.text = ""
        }
    }
}


extension GalleryVC: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            dismiss(animated:true, completion: nil)
        }
    }
}
