//  LocalNotifVC.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko


import UIKit
import UserNotifications

class LocalNotifVC: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    @IBAction func sendNotific(_ sender: Any) {
        
        let alertOneDo = UIAlertController(title: "Уведомления", message: "Выйдите из приложения и уведомление прийдет через 7 секунд", preferredStyle: .alert)
        alertOneDo.addAction(UIAlertAction(title: "ОК", style: .default) {
            (action) in
            self.appDelegate?.notifications.scheduleNotification()
        })
        present(alertOneDo, animated: true)
    }
    
}
