//  FontVC.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko


import UIKit

class FontVC: UIViewController {
    
    @IBOutlet weak var textTransformLable: UILabel!
    
    @IBAction func textTransformAction(_ sender: Any) {
        textTransformLable.font = UIFont(name: "AkayaKanadaka-Regular", size: 22)
    }
    
}
