//  ViewController.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko


import UIKit
import CoreMotion

class SensorVC: UIViewController {
    
    @IBOutlet weak var lableProximitySensorOutlet: UITextField!
    @IBOutlet weak var lableAcsilirometrOutlet: UITextField!
    
    let motionManager = CMMotionManager()
    
    @IBAction func startSensorReadAction(_ sender: Any) {
        activateProximitySensor()
        activateAccelerometer()
    }
    
    
    func activateProximitySensor() {
        let device = UIDevice.current
        device.isProximityMonitoringEnabled = true
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(proximityChanged(_:)),
                                               name: UIDevice.proximityStateDidChangeNotification,
                                               object: nil)
    }
    
    @objc func proximityChanged(_ notification: NSNotification) {
        let device = UIDevice.current
        
        if device.proximityState == true {
            lableProximitySensorOutlet.text = "Девайс у уха!"
        } else {
            lableProximitySensorOutlet.text = "Девайс далеко от уха!"
        }
    }
    
    
    func activateAccelerometer() {
        if motionManager.isAccelerometerAvailable {    // если доступ разрешен:
            motionManager.accelerometerUpdateInterval = 1   // время обновления данных 1 сек
            motionManager.startAccelerometerUpdates(to: OperationQueue.main) { (data, error) in  // с помощью этого метода и замыкания обрабатываем переданные данные
                if let data = data {
                    
                    let x = (data.acceleration.x * 100).rounded() / 100   // метод rounded() округляет до целого числа
                    let y = (data.acceleration.y * 100).rounded() / 100   // но тип остается Double
                    let z = (data.acceleration.z * 100).rounded() / 100
                    
                    self.lableAcsilirometrOutlet.text = "X: \(x), Y: \(y), Z: \(z)"
                }
            }
        }
    }
    
}

