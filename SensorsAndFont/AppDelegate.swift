//  AppDelegate.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko


import UIKit
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let notifications = Notifications()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //                // Получаем названия всех шрифтов:
        //                UIFont.familyNames.forEach({ familyName in
        //                    let fontNames = UIFont.fontNames(forFamilyName: familyName)
        //                    print(familyName, fontNames)
        //                })
        
        
        notifications.notificationCenter.delegate = notifications
        notifications.notificationRequest()
        
        return true
    }
}
