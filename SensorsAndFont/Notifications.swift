//  Notifications.swift
//  SensorsAndFont
//  Created by Viktoriia Skvarko

import UIKit
import Foundation
import UserNotifications

class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    // создаем центр нотификаций
    let notificationCenter = UNUserNotificationCenter.current()
    
    func notificationRequest() {
        
        // опции для уведомлений
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        // запрос у пользователя разрешения на отправку
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    
    func scheduleNotification() {
        
        // Содержимое уведомления:
        let content = UNMutableNotificationContent()
        content.title = "Новое уведомление"
        content.body = "Вам пришло тестовое уведомление из приложения по ДЗ №11"
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        // тригер - отправка уведомления через 7 секунд:
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 7, repeats: false)
        
        // создаем запрос на уведомление и добавляем его в центр уведомлений
        let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
}
